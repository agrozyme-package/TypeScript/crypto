import type { AsyncCipher, Cipher } from '@noble/ciphers/utils';
import type { Hex } from '@noble/curves/abstract/utils';
import type { ArgonOpts } from '@noble/hashes/argon2';
import type { Blake3Opts } from '@noble/hashes/blake3';
import type { ScryptOpts } from '@noble/hashes/scrypt';
import type { KangarooOpts } from '@noble/hashes/sha3-addons';
import type { Input } from '@noble/hashes/utils';

export interface Curve {
  keySize: number;
  randomPrivateKey: () => Uint8Array;
  getPublicKey: (privateKey: Hex, isCompressed?: boolean) => Uint8Array;
  sign: (message: Hex, privateKey: Hex) => Uint8Array;
  verify: (signature: Hex, message: Hex, publicKey: Hex) => boolean;
}

export type BlakeOpts = {
  dkLen?: number;
  key?: Input;
  salt?: Input;
  personalization?: Input;
};

export type HashOptions = ArgonOpts | BlakeOpts | Blake3Opts | ScryptOpts | KangarooOpts;
export type HashFunction = (message: Input) => Uint8Array;
export type TupleHashFunction = (messages: Input[]) => Uint8Array;
export type SaltHashFunction = (password: string, salt: string) => Uint8Array;
export type Argon2HashFunction = (password: Input, salt: Input, opts: ArgonOpts) => Uint8Array;

export type CipherFunction = (key: Uint8Array, nonce: Uint8Array) => Cipher;
export type AsyncCipherFunction = (key: Uint8Array, nonce: Uint8Array) => AsyncCipher;
