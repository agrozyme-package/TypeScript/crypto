import { p521 as p521Curve, secp521r1 as secp521r1Curve } from '@noble/curves/p521';
import { makeWeierstrassCurve } from './abstract';

const keySize = 66;
export const p521 = makeWeierstrassCurve(p521Curve, keySize);
export const secp521r1 = makeWeierstrassCurve(secp521r1Curve, keySize);
