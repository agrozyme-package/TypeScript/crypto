import { jubjub as jubjubCurve } from '@noble/curves/jubjub';
import { makeEdwardsCurve } from './abstract';

export const jubjub = makeEdwardsCurve(jubjubCurve, 32);
