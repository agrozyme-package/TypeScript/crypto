import type { Hex } from '@noble/curves/abstract/utils';
import { schnorr as schnorrCurve, secp256k1 as secp256k1Curve } from '@noble/curves/secp256k1';
import type { Curve } from '../types';
import { makeWeierstrassCurve } from './abstract';

const keySize = 32;

export const schnorr: Curve = {
  keySize,
  randomPrivateKey: schnorrCurve.utils.randomPrivateKey,
  getPublicKey: (privateKey: Hex, isCompressed?: boolean): Uint8Array => schnorrCurve.getPublicKey(privateKey),
  sign: schnorrCurve.sign,
  verify: schnorrCurve.verify,
};

export const secp256k1 = makeWeierstrassCurve(secp256k1Curve, keySize);
