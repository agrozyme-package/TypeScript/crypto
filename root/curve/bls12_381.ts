import type { Hex } from '@noble/curves/abstract/utils';
import { bls12_381 as bls12_381Curve } from '@noble/curves/bls12-381';
import type { Curve } from '../types';

export const bls12_381: Curve = {
  keySize: 32,
  randomPrivateKey: bls12_381Curve.utils.randomPrivateKey,
  getPublicKey: (privateKey: Hex, isCompressed?: boolean): Uint8Array => bls12_381Curve.getPublicKey(privateKey),
  sign: bls12_381Curve.sign,
  verify: bls12_381Curve.verify,
};
