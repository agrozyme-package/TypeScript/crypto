import { bn254_weierstrass } from '@noble/curves/bn254';
import { makeWeierstrassCurve } from './abstract';

export const bn254 = makeWeierstrassCurve(bn254_weierstrass, 32);
