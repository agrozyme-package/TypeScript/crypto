import type { Hex } from '@noble/curves/abstract/utils';
import type { CurveFn } from '@noble/curves/abstract/weierstrass';
import type { Curve } from '../../types';

export const makeWeierstrassCurve = (curve: CurveFn, keySize: number): Curve => ({
  keySize,
  randomPrivateKey: curve.utils.randomPrivateKey,
  getPublicKey: curve.getPublicKey,
  sign: (message: Hex, privateKey: Hex): Uint8Array => curve.sign(message, privateKey).toCompactRawBytes(),
  verify: curve.verify,
});
