import { ed448 as ed448Curve, ed448ph as ed448phCurve } from '@noble/curves/ed448';
import { makeEdwardsCurve } from './abstract';

const keySize = 57;
export const ed448 = makeEdwardsCurve(ed448Curve, keySize);
export const ed448ph = makeEdwardsCurve(ed448phCurve, keySize);
